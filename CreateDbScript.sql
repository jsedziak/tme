CREATE DATABASE NumberGenerator;
GO
USE [NumberGenerator];
GO
SELECT TOP (9999999 - 1000000 + 1) IDENTITY(int,1000000,1) AS Number, -1 AS OrderIdx
    INTO UsedNumbers
    FROM sys.objects s1      
    CROSS JOIN sys.objects s2 	
    CROSS JOIN sys.objects s3 	
    CROSS JOIN sys.objects s4
	ALTER TABLE UsedNumbers ADD CONSTRAINT PK_UsedNumbers PRIMARY KEY CLUSTERED (Number)