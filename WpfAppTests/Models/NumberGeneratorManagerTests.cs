﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace WpfApp.Models.Tests
{
    [TestClass()]
    public class NumberGeneratorManagerTests
    {
        uint howMany;
        List<int> unusedNumbers;
        List<int> generatedNumbers;
        NumberGeneratorManager numMgr;
        public NumberGeneratorManagerTests()
        {
            howMany = 2;
            numMgr = new NumberGeneratorManager();
            unusedNumbers = new List<int>() { 1, 2, 3 };
            generatedNumbers = new List<int>();
        }
        [TestMethod()]
        public void GenerateManyNumbersTest()
        {
            generatedNumbers.Clear();
            generatedNumbers = numMgr.GenerateManyNumbers(howMany,unusedNumbers);

            Assert.IsTrue(unusedNumbers.Count() == 1 && generatedNumbers.Count() == 2);
            Assert.IsTrue(generatedNumbers.Contains(1) || generatedNumbers.Contains(2) || generatedNumbers.Contains(3));
        }

        [TestMethod()]
        public void GenerateMoreNumbersThanRangeTest()
        {
            generatedNumbers.Clear();
            generatedNumbers = numMgr.GenerateManyNumbers(5, unusedNumbers);
            Assert.IsTrue(unusedNumbers.Count() == 0 && generatedNumbers.Count() == 3);
            Assert.IsTrue(generatedNumbers.Contains(1) && generatedNumbers.Contains(2) && generatedNumbers.Contains(3));
        }
    }
}