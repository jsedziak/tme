﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace WpfApp.Models.Tests
{
    [TestClass()]
    public class NumberGenratorTests
    {
        int upperRange;
        IndexGenerator generator;
        public NumberGenratorTests()
        {
            upperRange = 1;
        }
        [TestMethod()]
        public void GenerateNumberTest()
        {
            generator = new IndexGenerator();
            var num = -1;
            num = generator.GenerateNumber(upperRange);
            Assert.IsTrue(num == 0);
        }
    }
}