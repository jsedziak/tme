﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using WpfApp.Models;
using WpfApp.Models.Database;

namespace WpfApp.ViewModels
{
    public class ShellViewModel : Conductor<object>
    {
        #region properties
        private uint _howMany;

        public uint HowMany
        {
            get { return _howMany; }
            set
            {
                _howMany = value;
                NotifyOfPropertyChange(() => HowMany);
            }
        }

        public List<int> UnusedNumbers { get; set; }

        string _generatedNumbers;
        public string GeneratedNumbers
        {
            get => _generatedNumbers;
            set
            {
                _generatedNumbers = value;
                NotifyOfPropertyChange(() => GeneratedNumbers);
            }
        }
        IGenerateMany genMgr;
        private int maximumProgress;
        public int MaximumProgress
        {
            get { return maximumProgress; }
            set
            {
                if (maximumProgress == value)
                {
                    return;
                }

                maximumProgress = value;
                NotifyOfPropertyChange(() => MaximumProgress);
            }
        }
        private int _currentProgress;

        public int CurrentProgress
        {
            get { return _currentProgress; }
            set
            {
                _currentProgress = value;
                NotifyOfPropertyChange(() => CurrentProgress);
                //   NotifyOfPropertyChange(() => UsedDb); //Poor performance
            }
        }
        private DbManager _dbMg;
        public DbManager DbMgr
        {
            get => _dbMg;
            set
            {
                _dbMg = value;
                NotifyOfPropertyChange(UsedDb);
            }
        }
        public string UsedDb
        {
            get { return $"Wykorzystanie zakresu: {UsedPercent.ToString("F3")}%"; }
        }
        private double UsedPercent { get => DbMgr == null ? 0.0 : ((double)DbMgr.UsedNumbersCount() * 100 / (double)DbMgr.TotalNumbersCount()); }
        private int iterations { get; set; }
        #endregion
        #region Methods
        public ShellViewModel()
        {
            genMgr = new NumberGeneratorManager();
            DbMgr = new DbManager();
            UnusedNumbers = DbMgr.UnusedNumbers();
            genMgr.NewNumberAdded += GenMgr_NewNumberAdded;
            MaximumProgress = 1;
        }

        public void Generate()
        {
            ThreadPool.QueueUserWorkItem(w =>
            {
                CurrentProgress = 0;
                iterations = (int)HowMany < DbMgr.TotalNumbersCount() ? (int)HowMany : DbMgr.TotalNumbersCount();
                MaximumProgress = iterations / Divider(iterations);

                var generatedList = genMgr.GenerateManyNumbers(howMany: HowMany, divider: Divider(iterations), unusedNumbers: UnusedNumbers);
                var stb = new StringBuilder();
                foreach (var item in generatedList)
                {
                    stb.AppendLine(item.ToString());
                }
                GeneratedNumbers += stb.ToString();
                DbMgr.ShrinkRange(generatedList);
            });
        }
        public void Clean()
        {
            GeneratedNumbers = "";
        }
        public void GetAll()
        {
            GeneratedNumbers = DbMgr.GeneratedNumbersStr();
        }
        private int Divider(int iterations,int divider = 1)
        {
            if (((1-iterations) / 100) == 0)
            {
                return divider;
            }
            return Divider(iterations / 10, divider * 10);
        }
        private void GenMgr_NewNumberAdded(object o, EventArgs str)
        {
            CurrentProgress++;
        }
        #endregion
    }
}
