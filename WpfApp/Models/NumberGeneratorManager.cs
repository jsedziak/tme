﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace WpfApp.Models
{
    public class NumberGeneratorManager : IGenerateMany
    {
        #region Properties
        public IGenerateNumber idxGen { get; private set; }
        private List<int> _numberList;
        public List<int> UnusedNumberList
        {
            get => _numberList;
            set => _numberList = value;
        }
        #endregion
        #region Events
        public event EventHandler NewNumberAdded;
        #endregion
        #region Methods

        public List<int> GenerateManyNumbers(uint howMany, List<int> unusedNumbers, int? divider = null)
        {
            UnusedNumberList = unusedNumbers;
            idxGen = new IndexGenerator();
            int count = UnusedNumberList.Count;
            var usedNumList = new List<int>();
            var iterations = (int)howMany > count ? count : (int)howMany;
            divider = divider ?? (100 < iterations ? 10 : 1000 < iterations ? 100 : 1000);
            for (int i = 0; i < howMany && count > 0; i++, count--)
            {
                var idx = idxGen.GenerateNumber(count);
                usedNumList.Add(UnusedNumberList[idx]);
                UnusedNumberList.RemoveAt(idx);
                if (i != 0 && divider != null && (i % divider == 0 || i == iterations - 1))
                {
                    ThreadPool.QueueUserWorkItem(w =>
                    {
                        NewNumberAdded?.Invoke(this, new EventArgs());
                    });
                }
            }
            return usedNumList;
        }
        #endregion
    }
}
