﻿using System;
using System.Collections.Generic;

namespace WpfApp.Models
{
    public interface IGenerateMany
    {
        List<int> GenerateManyNumbers(uint howMany, List<int> unusedNumbers, int? divider = null);
        event EventHandler NewNumberAdded;
    }
}