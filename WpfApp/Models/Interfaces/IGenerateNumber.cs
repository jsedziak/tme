﻿using System.Collections.Generic;

namespace WpfApp.Models
{
    public interface IGenerateNumber
    {
        int GenerateNumber(int lastIdx);
    }
}