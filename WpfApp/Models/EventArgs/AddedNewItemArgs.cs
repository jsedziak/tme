﻿using System;

namespace WpfApp.Models
{
    public class AddedNewItemArgs : EventArgs
    {
        public int Item { get; set; }
        public AddedNewItemArgs(int item)
        {
            Item = item;
        }

        public override string ToString()
        {
            return Item.ToString();
        }
    }
}