﻿using System;
using System.Collections.Generic;

namespace WpfApp.Models
{
    public class IndexGenerator : IGenerateNumber
    {
        Random r = new Random();      

        public int GenerateNumber(int lastIdx)
        {
            return r.Next(0, lastIdx);
        }
    }
}
