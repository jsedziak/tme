﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WpfApp.Models.Database
{
    public class DbManager
    {
        #region PublicMethods
        public int TotalNumbersCount()
        {
            int count;
            using (var ef = new NumberGeneratorEntities())
            {
                count = ef.UsedNumbers.Count();
            }
            return count;
        }
        public int UsedNumbersCount()
        {
            int count;
            using (var ef = new NumberGeneratorEntities())
            {
                count = ef.UsedNumbers.Count(s => s.OrderIdx != -1);
            }
            return count;
        }

        public string GeneratedNumbersStr()
        {
            var stb = new StringBuilder();
            using (var ef = new NumberGeneratorEntities())
            {
                var numbers = ef.UsedNumbers.Where(w => w.OrderIdx != -1).OrderBy(o => o.OrderIdx).Select(s => s.Number);

                foreach (var item in numbers)
                {
                    stb.AppendLine(item.ToString());
                }
            }
            return stb.ToString();
        }
        #endregion
        #region PrivateMethods
        public List<int> UnusedNumbers()
        {
            var list = new List<int>();
            using (var ef = new NumberGeneratorEntities())
            {
                list = ef.UsedNumbers.Where(w => w.OrderIdx == -1).Select(s => s.Number).ToList();
            }
            return list;
        }



        public void ShrinkRange(List<int> generatedList)
        {
            using (var ef = new NumberGeneratorEntities())
            {
                var maxIdx = ef.UsedNumbers.Max(m => m.OrderIdx);
                foreach (var item in generatedList)
                {
                    var num = ef.UsedNumbers.First(w => w.Number == item);
                    num.OrderIdx = ++maxIdx;
                }
                ef.SaveChanges();
            }
        }
        #endregion

    }
}
